Wiqi
====

Potential Issues
----------------

Wiqi will attempt to pull in a stylesheet from /style.css, to mimic the
parent site's style if it's hosted at a subpath (eg, /wiki/). However,
if the background color of the parent site is set on the "html" tag,
not the "body", rendering may be unseemly.

Best practice advice suggests setting background settings on the
body, which should resolve this issue. Alternatively, override
.wiqi.actions-bar's background style manually in the parent stylesheet.


