#!/usr/bin/python3
import sys
import sqlite3
import time
import re
from markdown import markdown
from bottle import run, route, request, response, redirect, error, html_escape, html_quote, HTTPResponse
from lxml.html.clean import Cleaner  # pip install --user lxml
from wsgiref.handlers import format_date_time

wiqi_version = "0.6"
schema_version = 2

#------------ Web -------------

@error(404)
def error404(error):
  return makepage("Not found", "", "Not found")

@route('<file:re:^/(robots\\.txt|style\\.css|links\\.js)$>')
def serve_static_routes(file):
  response.add_header("Cache-Control", "max-age=86400")
  response.content_type = static_files[file][0]
  return static_files[file][1]

@route("/")
def homepage():
  return redirect("home/")

@route("/<name>")
def handle_redirect(name):
  return redirect("./" + name + "/")

@route("/<name>/")
def show_page(name):
  (page, rev, exists) = get_page(db, name)
  response.add_header("Cache-Control", "max-age=3600")
  if not exists:
    return HTTPResponse(status=404, body=makepage("Not Found", "<a href='./_edit'>Create</a>", "Not Found <a href='./_edit'>Create?</a>"))
  if request.method == "HEAD":
    return ""
  (source, render, date, _) = get_revision(db, page, rev)
  # TODO: Add ETags
  return makepage(name, "<a href='./_edit'>Edit</a> | <a href='./_history'>History</a>",  render + "<script src='" + basepath(request) + "links.js'></script>")

@route("/<name>/_history")
def get_history(name):
  (page, _, exists) = get_page(db, name)
  if not exists:
    return redirect('.')

  revid = request.params.rev
  preview = request.params.preview
  if revid is '':
    return makepage(name + " - History", "<a href='./'>Back to View</a>", "Revisions<br/>" + makehistorylist(db, page))
  return makepage(name + " - Revision #" + revid, "<a href='./_history'>Revisions</a> | <a href='./'>Current Version</a>", makehistoryview(db, page, revid, preview))

@route("/<name>/_edit")
def get_edit_page(name):
  (page, rev, exists) = get_page(db, name)
  source = ''
  if exists:
     (source, _, _, _)= get_revision(db, page, rev) 
  return makepage(name + " - Edit", "<a href='./'>Back to View</a>", "<form method='post'><input type='text' name='newname' value='" + html_escape(name) + "'><textarea type='text' name='newsource' rows=30 cols=80>" + html_escape(source) + "</textarea><input type='text' name='message' placeholder='What did you change?'/><input type='submit' name='submit' value='Update' style='display:block'/></form>")

@route("/<name>/_edit", method="POST")
def put_edit(name):
  user = getuser()
  newsource = request.params.get('newsource')
  message = request.params.get('message').strip() or "<No message>"
  newname = request.params.get('newname', "").strip()
  if not checkpagename(newname):
    return makepage("Error", "", "Invalid page name: " + html_escape(newname) + ". Please use the back button and enter a valid name. Valid characters are: alphanumeric, dashes, underscores, and colons.")
  (page, rev, exists) = get_page(db, name)
  if not exists:
    create_page(db, name, user)
    (page, rev, exists) = get_page(db, name)
  put_revision(db, page, rev + 1, newsource, render(newsource), user, message, newname)
  return redirect(basepath(request) + html_escape(newname) + '/')

@route("/_index/")
def get_index():
  pages = get_pages(db)
  result = "".join(("<li><a href='../" + page[0] + "'>" + page[0] + "</a></li>") for page in pages)
  return makepage("Index", "", "<ul>" + result + "</ul>")

@route("/_version/")
def get_version():
  return makepage("Version", "", "Version: " + str(wiqi_version) + "<br/>Schema: " + str(schema_version))
      
def makehistoryview(db, pageid, revid, preview):
  result = get_revision(db, pageid, revid)
  if result is None:
    return "Not Found"
  (source, render, _, _) = result
  if preview:
    return "Revision " + revid + " (preview)<hr/>" + render
  return "Revision " + revid + " (source)<hr/><textarea type='text' rows=30 cols=80>" + html_escape(source) + "</textarea>"
  
def render(source):
  return Cleaner().clean_html(markdown(source).strip() or "<p></p>")

def getuser():
  return html_escape(request.auth[0]) if request.auth else 'anonymous'

def checkpagename(name):
  return re.match(r"^[A-Za-z0-9-][A-Za-z0-9-_:]*$", name)

def makehistorylist(db, pageid):
  rows = get_revisions(db, pageid)
  result = ""
  lastname = ""
  for (date, message, user, rev, name) in rows:
    result = ("<li value='" + str(rev) + "'><span class='date' title='" + str(date) + "'>" + str(date)[:10] + "</span> " + 
      " <a href='_history?preview=yes&rev=" + str(rev) + "'>" + html_escape(message) + "</a> " +
      "<span class='user'>" + user + "</span> (<a href='_history?rev=" + str(rev) + "'>Source</a>) " +
      ("" if name == lastname else "<br/>(" + lastname + " -> " + name + ")") + 
      result)
    lastname = name
  return "<ol>" + result + "</ol>"

def makepage(heading, actions, body):
  baseurl = basepath(request)
  return (
    "<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=yes'>" + 
    "<link rel='stylesheet' href='" + baseurl + "style.css'>" +
    "<link rel='stylesheet' href='/style.css'>" +
    "<title>" + html_escape(heading) + "</title>" + 
    "<div id='content' class='wiqi wrapper'>" +
    body + 
    "</div>" + 
    "<div class='wiqi actions-bar'>" +
    "<div class='wiqi actions-global' style='float:right;padding-bottom:5px'><a href='" + baseurl + "_index/'>Index</a> | <a href='" + baseurl + "home/'>Home</a></div>"
    "<div class='wiqi actions-local'>" + actions + "</div>" + 
    "</div>" + 
    "<div class='wiqi bottom-padding' style='height:100px;'></div>")

def basepath(request):
  return "./" + "../" * (request.path.count('/') - 1)

#------------ DB -------------

def _get_results(db, query, params):
  cur = db.cursor()
  cur.execute(query, params)
  return cur.fetchall()

def _get_first_result(db, query, params):
  rows = _get_results(db, query, params)
  return rows[0] if len(rows) > 0 else None

def get_page(db, name):
  info = _get_first_result(db, "SELECT page, revision FROM pages WHERE name = ?;", (name,))
  return (-1, -1, False) if not info else (info[0], info[1], True)

def get_pages(db):
  return _get_results(db, "SELECT name, page FROM pages ORDER BY upper(name) ASC",())

def get_revision(db, pageid, rev):
  return _get_first_result(db, "SELECT source, render, date, user FROM revisions WHERE page = ? AND revision = ?", (pageid, rev,))

def get_revisions(db, pageid):
  return _get_results(db, "SELECT date, message, user, revision, pagename FROM revisions WHERE page = ? ORDER BY date ASC, revision ASC", (pageid,))

def create_page(db, name, user):
  new_id = _get_first_result(db, "SELECT page FROM pages ORDER BY page DESC LIMIT 1", ())
  new_id = new_id[0] + 1 if new_id is not None else 0
  db.cursor().execute("INSERT INTO pages(name, page, revision) VALUES (?, ?, -1)", (name, new_id,))

def put_revision(db, pageid, rev, source, render, user, message, pagename):
  cur = db.cursor()
  cur.execute("INSERT INTO revisions(page, revision, date, user, source, render, message, pagename) " + 
      "VALUES (?, ?, (DATETIME('now')), ?, ?, ?, ?, ?)", (pageid, rev, user, source, render, message, pagename,))
  cur.execute("UPDATE pages SET revision = ?, name = ? WHERE page = ?", (rev, pagename, pageid,))
  db.commit()

#------------ INIT -------------

def init():
  global db
  if len(sys.argv) < 3:
    print("Usage: ./serve-wiki.py <sqlitedb> <port>")
    sys.exit(1)
  db = sqlite3.connect(sys.argv[1])
  if not ensure_schema(db):
    print("This database has been modified by a newer version of wiqi.")
    sys.exit(1)
  run(port=sys.argv[2])

#--------- Appendices ----------

static_files = {}

static_files["/style.css"] = ("text/css", """
.wrapper {
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
}

body {
  background: white;
}

.actions-bar {
  background: inherit;
  position: fixed;
  bottom: 0px;
  width: 95%;
  border-top: 1px solid;
}

.actions-local {
  float: left;
}

.actions-global: {
  float: right;
  padding-bottom: 5px;
}

.broken-link {
  color: red !important;
}
""")

static_files["/links.js"] = ("application/javascript", """
var links = Array.from(document.getElementById('content').getElementsByTagName('a'));
var domain = location.protocol + "//" + location.hostname;
links.filter(url => url.href.startsWith(domain)).forEach(testLink);

function testLink(link) {
  fetch(link.href, { method: 'HEAD' }).catch(e => e)
    .then(data => {
      if (!data.ok)
        link.classList.add('broken-link');
    });
}
""")

static_files["/robots.txt"] = ("text/plain", """
# Allow indexing, but not "backend" pages
User-Agent: *
Disallow: *_*
""")

def ensure_schema(db):
  cur = db.cursor()
  cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='wiki_version'")
  rows = cur.fetchall()
  schema = 0 if len(rows) < 1 else int(_get_first_result(db, "SELECT version FROM wiki_version", ())[0])

  if schema > schema_version:
    return False

  if schema < 1:
    db.cursor().execute("CREATE TABLE wiki_version(version integer);")
    db.cursor().execute("CREATE TABLE pages(name text, page integer, revision integer);")
    db.cursor().execute("CREATE TABLE revisions(page integer, revision integer, date integer, user text, source text, render text);")
    db.cursor().execute("CREATE UNIQUE INDEX pages_index ON pages(name);")
    db.cursor().execute("CREATE UNIQUE INDEX revisions_index ON revisions(page, revision);")
    db.cursor().execute("INSERT INTO wiki_version(version) VALUES (1);")
    db.commit()

  if schema < 2:
    db.cursor().execute("ALTER TABLE revisions ADD message string;")
    db.cursor().execute("ALTER TABLE revisions ADD pagename string;")
    db.cursor().execute("UPDATE revisions SET message = '<No message>' WHERE message IS NULL");
    db.cursor().execute("UPDATE revisions AS r SET pagename = (SELECT name FROM pages AS p WHERE p.page = r.page)")
    db.cursor().execute("UPDATE wiki_version SET version = 2;")
    db.commit()

  return True

if __name__ == "__main__": 
  init()

